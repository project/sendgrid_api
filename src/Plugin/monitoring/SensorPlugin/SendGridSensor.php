<?php

declare(strict_types=1);

namespace Drupal\sendgrid_api\Plugin\monitoring\SensorPlugin;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\DependencyTrait;
use Drupal\key\KeyRepositoryInterface;
use Drupal\monitoring\Entity\SensorConfig;
use Drupal\monitoring\Result\SensorResultInterface;
use Drupal\monitoring\SensorPlugin\SensorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Checks if an API key is valid.
 *
 * @SensorPlugin(
 *   id = \Drupal\sendgrid_api\Plugin\monitoring\SensorPlugin\SendGridSensor::PLUGIN_ID,
 *   label = @Translation("SendGrid API Key"),
 *   description = @Translation("Checks connectivity and validity of a SendGrid API key."),
 *   addable = TRUE,
 *   deriver = "Drupal\sendgrid_api\Plugin\Derivative\SendGridSensorDeriver"
 * )
 */
class SendGridSensor extends SensorPluginBase implements DependentPluginInterface {

  use DependencyTrait;

  public const PLUGIN_ID = 'sendgrid_key';

  /**
   * SendGridSensor constructor.
   *
   * @param \Drupal\monitoring\Entity\SensorConfig $sensorConfig
   *   Sensor config object.
   * @param string $pluginId
   *   The plugin_id for the plugin instance.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\key\KeyRepositoryInterface $keyRepository
   *   Repository for Key configuration entities.
   * @param \SendGrid $sendGrid
   *   SendGrid API client.
   */
  final public function __construct(
    SensorConfig $sensorConfig,
    $pluginId,
    $pluginDefinition,
    protected KeyRepositoryInterface $keyRepository,
    protected \SendGrid $sendGrid,
  ) {
    parent::__construct($sensorConfig, $pluginId, $pluginDefinition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, SensorConfig $sensor_config, $plugin_id, $plugin_definition): static {
    return new static(
      $sensor_config,
      $plugin_id,
      $plugin_definition,
      $container->get('key.repository'),
      $container->get('sendgrid_api.client'),
    );
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return array<string, mixed>
   */
  public function calculateDependencies(): array {
    [1 => $keyId] = \explode(':', $this->getPluginId(), 2);
    $key = $this->keyRepository->getKey($keyId);
    if ($key !== NULL) {
      $this->addDependency('config', $key->getConfigDependencyName());
    }
    return $this->dependencies;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return array<string, mixed>
   */
  public function getDefaultConfiguration(): array {
    return [
      'caching_time' => 3600,
      'value_type' => 'bool',
      'category' => 'SendGrid API',
      'settings' => [],
    ];
  }

  public function runSensor(SensorResultInterface $sensor_result): void {
    $sensor_result->setValue(FALSE);
    $sensor_result->setExpectedValue(TRUE);

    [1 => $keyId] = \explode(':', $this->getPluginId(), 2);

    $key = $this->keyRepository->getKey($keyId);
    if ($key === NULL) {
      $sensor_result->setMessage('Missing key');
      return;
    }

    $apiKey = $key->getKeyValue();
    $sendGrid = new \SendGrid($apiKey);

    try {
      $response = $sendGrid->client->user()->username()->get();
      $json = $response->body();
      /** @var array<string, mixed> $decoded */
      $decoded = Json::decode($json);
      $userName = $decoded['username'] ?? NULL;
      if ($userName) {
        $sensor_result->setValue(TRUE);
        $sensor_result->setMessage('Logged in as @username.', [
          '@username' => $userName,
        ]);
      }
      else {
        $sensor_result->setMessage('Unexpected response from API');
      }
    }
    catch (\Exception $exception) {
      $sensor_result->setMessage(\sprintf('Error: %s', $exception->getMessage()));
    }
  }

}
