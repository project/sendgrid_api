<?php

declare(strict_types=1);

namespace Drupal\sendgrid_api\Exception;

/**
 * Generic SendGrid exception interface.
 */
interface SendGridApiExceptionInterface extends \Throwable {

}
