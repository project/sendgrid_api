<?php

declare(strict_types=1);

// phpcs:disable Drupal.NamingConventions.ValidFunctionName.ScopeNotCamelCaps

namespace Drupal\sendgrid_api;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Utils;
use SendGrid\Client;
use SendGrid\Response;

/**
 * Overrides SendGrid client adding Guzzle support.
 */
class SendGridClient extends Client {

  /**
   * HTTP client.
   */
  protected ClientInterface $httpClient;

  /**
   * Constructs a new client using an existing client as a base.
   *
   * @param \SendGrid\Client $client
   *   An existing client.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   HTTP client.
   *
   * @return \Drupal\sendgrid_api\SendGridClient
   *   A new SendGrid client using Guzzle.
   */
  final public static function createFromClient(Client $client, ClientInterface $httpClient): SendGridClient {
    $instance = new self(
      $client->host,
      $client->headers,
      $client->version,
      $client->path,
      $client->curlOptions,
      $client->retryOnLimit,
    );
    $instance->setHttpClient($httpClient);
    return $instance;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param string|null $name
   */
  public function _($name = NULL) {
    /** @var static $client */
    $client = parent::_($name);
    // Push Guzzle down the chain.
    $client->setHttpClient($this->httpClient);
    return $client;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param array<string, mixed>|null $body
   * @phpstan-param array<string, mixed>|null $headers
   */
  public function makeRequest($method, $url, $body = NULL, $headers = NULL, $retryOnLimit = FALSE) {
    $headers = $headers ?? [];

    // @todo implement $retryOnLimit.
    $options = [];

    // Body.
    // @see \SendGrid\Client::createCurlOptions().
    if (isset($body)) {
      $encodedBody = \json_encode($body);
      $headers[] = 'Content-Type: application/json';
      $options['body'] = $encodedBody;
    }

    $headers = \array_merge($this->headers, $headers);
    $options['headers'] = Utils::headersFromLines($headers);

    $response = $this->httpClient->request($method, $url, $options);

    return new Response(
      $response->getStatusCode(),
      (string) $response->getBody(),
      $response->getHeaders(),
    );
  }

  /**
   * Sets HTTP client.
   *
   * @return $this
   *   Returns self for chaining.
   */
  protected function setHttpClient(ClientInterface $httpClient) {
    $this->httpClient = $httpClient;
    return $this;
  }

}
