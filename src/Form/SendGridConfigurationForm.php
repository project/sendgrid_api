<?php

declare(strict_types=1);

namespace Drupal\sendgrid_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * SendGrid API configuration form.
 */
final class SendGridConfigurationForm extends ConfigFormBase {

  public function getFormId(): string {
    return 'sendgrid_api_configuration_form';
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-return array<string>
   */
  protected function getEditableConfigNames(): array {
    return ['sendgrid_api.settings'];
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param array<string, mixed> $form
   * @phpstan-return array<string, mixed>
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('sendgrid_api.settings');

    $form['key'] = [
      '#type' => 'key_select',
      '#title' => $this->t('API key'),
      '#default_value' => $config->get('api_key'),
      '#config_data_store' => 'sendgrid_api.settings:api_key',
      '#key_filters' => [
        'type' => 'sendgrid_api_key',
      ],
    ];

    $form['use_guzzle'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Experimental: Use Guzzle shim'),
      '#description' => $this->t('This will use the HTTP client built into Drupal rather than direct calls to Curl.'),
      '#default_value' => $config->get('http_client_shim'),
      '#config_data_store' => 'sendgrid_api.settings:http_client_shim',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-param array<string, mixed> $form
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);
    $key = $form_state->getValue('key');
    $useGuzzle = $form_state->getValue('use_guzzle');
    $this->config('sendgrid_api.settings')
      ->set('api_key', $key)
      ->set('http_client_shim', $useGuzzle)
      ->save();
  }

}
