<?php

declare(strict_types=1);

namespace Drupal\Tests\sendgrid_api\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\key\Entity\Key;
use Drupal\sendgrid_api\Plugin\KeyType\SendGridKeyType;
use Drupal\sendgrid_api\SendGridApiKeyInterface;
use Drupal\sendgrid_api\SendGridFactory;

/**
 * Tests SendGrid.
 *
 * @group sendgrid_api
 */
final class SendGridApiTest extends KernelTestBase {

  protected static $modules = [
    'sendgrid_api',
    'key',
  ];

  /**
   * Integration test.
   *
   * Invoking the service causes Key and API key services to boot up.
   */
  public function testSendGrid(): void {
    Key::create([
      'id' => 'foo',
      'label' => 'Foo',
      'key_type' => SendGridKeyType::PLUGIN_ID,
      'key_type_settings' => [],
      'key_provider' => 'config',
      'key_provider_settings' => ['key_value' => 'my_key'],
    ])->save();

    \Drupal::configFactory()
      ->getEditable('sendgrid_api.settings')
      ->set('api_key', 'foo')
      ->save();
    self::assertInstanceOf(\SendGrid::class, \Drupal::service('sendgrid_api.client'));
    self::assertInstanceOf(\SendGrid::class, \Drupal::service(\SendGrid::class));

    // Inlined/private.
    static::assertFalse(\Drupal::hasService(SendGridApiKeyInterface::class));
    static::assertFalse(\Drupal::hasService(SendGridFactory::class));
  }

}
